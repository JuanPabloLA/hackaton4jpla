from django.urls import include, path
from django.contrib import admin
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from rest_framework import routers
from hack4web import views

router = routers.DefaultRouter()
router.register(r'Specialties', views.SpecialtiesViewSet)
router.register(r'Doctors', views.DoctorsViewSet)
router.register(r'Medical_speciality', views.Medical_specialityViewSet)
router.register(r'Patients', views.PatientsViewSet)
router.register(r'Appointments', views.AppointmentsViewSet)
router.register(r'Schedule', views.ScheduleViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    path('', include('hack4web.urls')),
    path('hack4web/token/', TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('hack4web/token/refresh/',
         TokenRefreshView.as_view(), name='token_refresh'),
]