from django.urls import path
from . import views

from .views import (AppointmentsCreateView, AppointmentsDeleteView, AppointmentsListView, AppointmentsUpdateView, DoctorsCreateView, 
DoctorsDeleteView, 
DoctorsListView, 
DoctorsUpdateView, 
MedicalSpecialityCreateView, 
MedicalSpecialityDeleteView, 
MedicalSpecialityListView, 
MedicalSpecialityUpdateView, 
PatientsCreateView, 
PatientsDeleteView, 
PatientsListView, 
PatientsUpdateView, ScheduleCreateView, ScheduleDeleteView, ScheduleListView, ScheduleUpdateView, 
SpecialtiesListView, 
SpecialtiesUpdateView, 
Success,
SpecialtiesCreateView)

app_name = 'hack4web'

urlpatterns = [
    path('success/', Success.as_view(), name='success'),

    path('create-specialty/', SpecialtiesCreateView.as_view(), name='create-specialty'),
    path('list-specialty/', SpecialtiesListView.as_view(), name='list-specialty'),
    path('update-specialty/<int:pk>/', SpecialtiesUpdateView.as_view(), name='update-specialty'),
    path('delete-specialty/<int:pk>/', views.SpecialtiesDeleteView.as_view(), name='delete-specialty'),

    path('create-doctor/', DoctorsCreateView.as_view(), name='create-doctor'),
    path('list-doctor/', DoctorsListView.as_view(), name='list-doctor'),
    path('update-doctor/<pk>/', DoctorsUpdateView.as_view(), name='update-doctor'),
    path('delete-doctor/<pk>/', DoctorsDeleteView.as_view(), name='delete-doctor'),

    path('create-medical-specialty/', MedicalSpecialityCreateView.as_view(), name='create-medical-specialty'),
    path('list-medical-specialty/', MedicalSpecialityListView.as_view(), name='list-medical-specialty'),
    path('update-medical-specialty/<pk>/', MedicalSpecialityUpdateView.as_view(), name='update-medical-specialty'),
    path('delete-medical-specialty/<pk>/', MedicalSpecialityDeleteView.as_view(), name='delete-medical-specialty'),

    path('create-patient/', PatientsCreateView.as_view(), name='create-patient'),
    path('list-patient/', PatientsListView.as_view(), name='list-patient'),
    path('update-patient/<pk>/', PatientsUpdateView.as_view(), name='update-patient'),
    path('delete-patient/<pk>/', PatientsDeleteView.as_view(), name='delete-patient'),

    path('create-appointment/', AppointmentsCreateView.as_view(), name='create-appointment'),
    path('list-appointment/', AppointmentsListView.as_view(), name='list-appointment'),
    path('update-appointment/<pk>/', AppointmentsUpdateView.as_view(), name='update-appointment'),
    path('delete-appointment/<pk>/', AppointmentsDeleteView.as_view(), name='delete-appointment'),

    path('create-schedule/', ScheduleCreateView.as_view(), name='create-schedule'),
    path('list-schedule/', ScheduleListView.as_view(), name='list-schedule'),
    path('update-schedule/<pk>/', ScheduleUpdateView.as_view(), name='update-schedule'),
    path('delete-schedule/<pk>/', ScheduleDeleteView.as_view(), name='delete-schedule'),
]