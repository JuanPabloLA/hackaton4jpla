from django import forms

from hack4web.models import (Specialties, 
Doctors, 
Medical_speciality, 
Patients, 
Appointments, 
Schedule)

class SpecialtiesForm(forms.ModelForm):
    class Meta:
        model = Specialties
        fields = ['Name',
                  'Description',
                  'Registration_date',
                  'Modification_date',
                  'User_registration',
                  'User_modification',
                  'Status',]


class DoctorsForm(forms.ModelForm):
    class Meta:
        model = Doctors
        fields = ['Name',
                  'Last_name',
                  'Dni',
                  'Address',
                  'Email',
                  'Phone',
                  'Gender',
                  'Specialty_id',
                  'University_number',
                  'Birth_date',
                  'Registration_date',
                  'Modification_date',
                  'User_registration',
                  'User_modification',
                  'Status',]


class Medical_specialityForm(forms.ModelForm):
    class Meta:
        model = Medical_speciality
        fields = ['Doctor_id',
                  'Speciality',
                  'Registration_date',
                  'Modification_date',
                  'User_registration',
                  'User_modification',
                  'Status',]


class PatientsForm(forms.ModelForm):
    class Meta:
        model = Patients
        fields = ['Name',
                  'Last_name',
                  'Dni',
                  'Address',
                  'Phone',
                  'Gender',
                  'Birth_date',
                  'Registration_date',
                  'Modification_date',
                  'User_registration',
                  'User_modification',
                  'Status',]


class AppointmentsForm(forms.ModelForm):
    class Meta:
        model = Appointments
        fields = ['Doctor_id',
                  'Patients_id',
                  'Date_care',
                  'Start_service',
                  'End_services',
                  'Status',
                  'Observations',
                  'Registration_date',
                  'Modification_date',
                  'User_registration',
                  'User_modification',]


class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = ['Doctor_id',
                  'Date_care',
                  'Start_service',
                  'End_services',
                  'Status',
                  'Registration_date',
                  'Modification_date',
                  'User_registration',
                  'User_modification',]