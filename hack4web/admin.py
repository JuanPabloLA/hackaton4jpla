from django.contrib import admin

from hack4web.models import Appointments, Doctors, Medical_speciality, Patients, Schedule, Specialties


# Register your models here.

@admin.register(Specialties)
class SpecialtiesAdmin(admin.ModelAdmin):
    list_display=(
        'Name',
        'Registration_date',
    )

@admin.register(Doctors)
class DoctorsAdmin(admin.ModelAdmin):
    list_display=(
        'Name',
        'Last_name',
    )

@admin.register(Medical_speciality)
class MedicalSpecialityAdmin(admin.ModelAdmin):
    list_display=(
        'Speciality',
    )

@admin.register(Patients)
class PatientsAdmin(admin.ModelAdmin):
    list_display=(
        'Name',
        'Last_name',
    )

@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display=(
        'id',
        'Date_care',
    )

@admin.register(Appointments)
class AppointmentsAdmin(admin.ModelAdmin):
    list_display=(
        'id',
        'Date_care',
    )