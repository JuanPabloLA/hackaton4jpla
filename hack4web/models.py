from tkinter import CASCADE
from django.db import models

# Create your models here.

class Specialties(models.Model):
    Name = models.CharField(max_length=30)
    Description = models.CharField(max_length=30)
    Registration_date = models.DateField(auto_now_add=False)
    Modification_date = models.DateField(auto_now_add=False)
    User_registration = models.CharField(max_length=30)
    User_modification = models.CharField(max_length=30)
    Status = models.BooleanField(default=True)

    def __str__(self):
        return self.Name

class Doctors(models.Model):
    GENDER_CHOICES = [
        ('MALE', 'Male'),
        ('FEMALE', 'Female')
    ]
    Male = 'Male'
    Female = 'Female'
    Name = models.CharField(max_length=30)
    Last_name = models.CharField(max_length=30)
    Dni = models.CharField(max_length=10)
    Address = models.CharField(max_length=50)
    Email = models.CharField(max_length=50)
    Phone = models.CharField(max_length=10)
    Gender = models.CharField(max_length=6,choices=GENDER_CHOICES)
    Specialty_id = models.ForeignKey(Specialties, on_delete=models.CASCADE)
    University_number = models.CharField(max_length=10)
    Birth_date = models.DateField(auto_now_add=False)
    Registration_date = models.DateField(auto_now_add=False)
    Modification_date = models.DateField(auto_now_add=False)
    User_registration = models.CharField(max_length=30)
    User_modification = models.CharField(max_length=30)
    Status = models.BooleanField(default=True)

    def __str__(self):
        return self.Name

class Medical_speciality(models.Model):
    Doctor_id = models.ForeignKey(Doctors, on_delete=models.CASCADE)
    Speciality = models.ForeignKey(Specialties, on_delete=models.CASCADE)
    Registration_date = models.DateField(auto_now_add=False)
    Modification_date = models.DateField(auto_now_add=False)
    User_registration = models.CharField(max_length=30)
    User_modification = models.CharField(max_length=30)
    Status = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.Doctor_id} {self.Speciality}'


class Patients(models.Model):
    GENDER_CHOICES = [
        ('MALE', 'Male'),
        ('FEMALE', 'Female')
    ]
    Name = models.CharField(max_length=30)
    Last_name = models.CharField(max_length=30)
    Dni = models.CharField(max_length=10)
    Address = models.CharField(max_length=50)
    Phone = models.CharField(max_length=10)
    Gender = models.CharField(max_length=6,choices=GENDER_CHOICES)
    Birth_date = models.DateField(auto_now_add=False)
    Registration_date = models.DateField(auto_now_add=False)
    Modification_date = models.DateField(auto_now_add=False)
    User_registration = models.CharField(max_length=30)
    User_modification = models.CharField(max_length=30)
    Status = models.BooleanField(default=True)

    def __str__(self):
        return self.Name

class Schedule(models.Model):
    Doctor_id = models.ManyToManyField(Doctors)
    Date_care = models.DateField(auto_now_add=False)
    Start_service = models.DateTimeField(auto_now_add=False)
    End_services = models.DateTimeField(auto_now_add=False)
    Status = models.BooleanField(default=True)
    Registration_date = models.DateField(auto_now_add=False)
    Modification_date = models.DateField(auto_now_add=False)
    User_registration = models.CharField(max_length=30)
    User_modification = models.CharField(max_length=30)

    def __str__(self):
        return f'{self.Doctor_id} {self.Date_care}'

class Appointments(models.Model):
    Doctor_id = models.ManyToManyField(Doctors)
    Patients_id = models.ManyToManyField(Patients)
    Date_care = models.DateField(auto_now_add=False)
    Start_service = models.DateTimeField(auto_now_add=False)
    End_services = models.DateTimeField(auto_now_add=False)
    Status = models.BooleanField(default=True)
    Observations = models.CharField(max_length=500)
    Registration_date = models.DateField(auto_now_add=False)
    Modification_date = models.DateField(auto_now_add=False)
    User_registration = models.CharField(max_length=30)
    User_modification = models.CharField(max_length=30)

    def __str__(self):
        return f'{self.Doctor_id} - {self.Patients_id} - {self.Date_care}'

