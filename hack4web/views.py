from multiprocessing.dummy import Process
from hack4web.forms import DoctorsForm, SpecialtiesForm
from rest_framework import viewsets
from hack4web.serializers import (SpecialtiesSerializer, 
DoctorsSerializer, 
Medical_specialitySerializer, 
PatientsSerializer, 
AppointmentsSerializer, 
ScheduleSerializer)
from hack4web.models import (Specialties, 
Doctors, 
Medical_speciality, 
Patients, 
Appointments, 
Schedule)
from django.views.generic import (CreateView, TemplateView, ListView, DeleteView,
                                  UpdateView)
from django.urls import reverse_lazy

""" Successfull Process """
class Success(TemplateView):
    template_name = "success.html"


""" Specialties ViewSet """
class SpecialtiesViewSet(viewsets.ModelViewSet):
    queryset = Specialties.objects.all()
    serializer_class = SpecialtiesSerializer

class SpecialtiesCreateView(CreateView):
    model = Specialties
    template_name = "specialties/create-specialties.html"
    form_class = SpecialtiesForm
    success_url = reverse_lazy('hack4web:success')

class SpecialtiesListView(ListView):
    model = Specialties
    context_object_name = 'list'
    template_name = "specialties/list-specialties.html"
    ordering = 'id'

class SpecialtiesUpdateView(UpdateView):
    model = Specialties
    template_name = "specialties/update-specialties.html"
    fields = ['Name', 'Description', 'Status']
    success_url = reverse_lazy('hack4web:success')

class SpecialtiesDeleteView(DeleteView):
    model = Specialties
    template_name = "specialties/delete-specialties.html"
    success_url = reverse_lazy('hack4web:success')
""" Specialties ViewSet """


""" Doctors ViewSet """
class DoctorsViewSet(viewsets.ModelViewSet):
    queryset = Doctors.objects.all()
    serializer_class = DoctorsSerializer

class DoctorsCreateView(CreateView):
    model = Doctors
    template_name = "doctors/create-doctors.html"
    form_class = DoctorsForm
    success_url = reverse_lazy('hack4web:success')

class DoctorsListView(ListView):
    model = Doctors
    context_object_name = 'list'
    template_name = "doctors/list-doctors.html"
    ordering = 'id'

class DoctorsUpdateView(UpdateView):
    model = Doctors
    template_name = "doctors/update-doctors.html"
    fields = ['Name', 'Last_name', 'Dni', 'Address', 'Email', 'Phone', 'Gender', 'Status',]
    success_url = reverse_lazy('hack4web:success')

class DoctorsDeleteView(DeleteView):
    model = Doctors
    template_name = "doctors/delete-doctors.html"
    success_url = reverse_lazy('hack4web:success')
""" Doctors ViewSet """


""" Medical speciality ViewSet """
class Medical_specialityViewSet(viewsets.ModelViewSet):
    queryset = Medical_speciality.objects.all()
    serializer_class = Medical_specialitySerializer

class MedicalSpecialityCreateView(CreateView):
    model = Medical_speciality
    template_name = "medicalspeciality/create-medical-speciality.html"
    form_class = Medical_specialityForm
    success_url = reverse_lazy('hack4web:success')

class MedicalSpecialityListView(ListView):
    model = Medical_speciality
    context_object_name = 'list'
    template_name = "medicalspeciality/list-medical-speciality.html"
    ordering = 'id'

class MedicalSpecialityUpdateView(UpdateView):
    model = Medical_speciality
    template_name = "medicalspeciality/update-medical-speciality.html"
    fields = ['Doctor_id', 'Speciality', 'Status',]
    success_url = reverse_lazy('hack4web:success')

class MedicalSpecialityDeleteView(DeleteView):
    model = Medical_speciality
    template_name = "medicalspeciality/delete-medical-speciality.html"
    success_url = reverse_lazy('hack4web:success')
""" Medical speciality ViewSet """


""" Patients ViewSet """
class PatientsViewSet(viewsets.ModelViewSet):
    queryset = Patients.objects.all()
    serializer_class = PatientsSerializer

class PatientsCreateView(CreateView):
    model = Patients
    template_name = "patients/create-patients.html"
    form_class = PatientsForm
    success_url = reverse_lazy('hack4web:success')

class PatientsListView(ListView):
    model = Patients
    context_object_name = 'list'
    template_name = "patients/list-patients.html"
    ordering = 'id'

class PatientsUpdateView(UpdateView):
    model = Patients
    template_name = "patients/update-patients.html"
    fields = ['Name', 'Last_name', 'Dni', 'Address', 'Phone', 'Gender', 'Status',]
    success_url = reverse_lazy('hack4web:success')

class PatientsDeleteView(DeleteView):
    model = Patients
    template_name = "patients/delete-patients.html"
    success_url = reverse_lazy('hack4web:success')
""" Patients ViewSet """


""" Appointments ViewSet """
class AppointmentsViewSet(viewsets.ModelViewSet):
    queryset = Appointments.objects.all()
    serializer_class = AppointmentsSerializer

class AppointmentsCreateView(CreateView):
    model = Appointments
    template_name = "appointments/create-appointments.html"
    form_class = AppointmentsForm
    success_url = reverse_lazy('hack4web:success')

class AppointmentsListView(ListView):
    model = Appointments
    context_object_name = 'list'
    template_name = "appointments/list-appointments.html"
    ordering = 'id'

class AppointmentsUpdateView(UpdateView):
    model = Appointments
    template_name = "appointments/update-appointments.html"
    fields = ['Doctor_id', 'Patients_id', 'Date_care', 'Start_service', 'End_services', 'Observations', 'Status',]
    success_url = reverse_lazy('hack4web:success')

class AppointmentsDeleteView(DeleteView):
    model = Appointments
    template_name = "appointments/delete-appointments.html"
    success_url = reverse_lazy('hack4web:success')
""" Appointments ViewSet """


""" Schedule ViewSet """
class ScheduleViewSet(viewsets.ModelViewSet):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer

class ScheduleCreateView(CreateView):
    model = Schedule
    template_name = "schedule/create-schedule.html"
    form_class = ScheduleForm
    success_url = reverse_lazy('hack4web:success')

class ScheduleListView(ListView):
    model = Schedule
    context_object_name = 'list'
    template_name = "schedule/list-schedule.html"
    ordering = 'id'

class ScheduleUpdateView(UpdateView):
    model = Schedule
    template_name = "schedule/update-schedule.html"
    fields = ['Doctor_id', 'Date_care', 'Start_service', 'End_services', 'Status',]
    success_url = reverse_lazy('hack4web:success')

class ScheduleDeleteView(DeleteView):
    model = Schedule
    template_name = "schedule/delete-schedule.html"
    success_url = reverse_lazy('hack4web:success')
""" Schedule ViewSet """