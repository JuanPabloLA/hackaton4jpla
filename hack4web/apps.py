from django.apps import AppConfig


class Hack4WebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hack4web'
